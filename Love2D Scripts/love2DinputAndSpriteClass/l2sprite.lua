L2Sprite = {}
L2Sprite.__index = L2Sprite

function L2Sprite:create(sprite, x, y)
   local l2sprite = {}
   setmetatable(l2sprite, L2Sprite)
   l2sprite.sprite = love.graphics.newImage(sprite)
   l2sprite.spriteFile = sprite
   l2sprite.x = x
   l2sprite.y = y
   return l2sprite
end

function L2Sprite:draw()
   love.graphics.draw(self.sprite, self.x, self.y)
end

function L2Sprite:moveTo(x, y)
    self.x = x
    self.y = y
end    