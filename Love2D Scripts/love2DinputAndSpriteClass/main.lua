require("l2sprite")

local whale

function love.load()
    whale = L2Sprite:create(#arg > 1 and arg[2] or "whale.jpg", 300, 200)
end

local posX = 300
local posY = 200
local velocity = 250 

function love.update(dt)
    if love.keyboard.isDown("escape") then 
        love.window.close() 
        love.event.quit(0)
        return
    end

    velocity = 250
    if(love.keyboard.isDown("rshift") or love.keyboard.isDown("lshift")) then
        velocity = 500
    end    

    if love.keyboard.isDown("up") then posY = posY -    velocity*dt end
    if love.keyboard.isDown("down") then posY = posY +  velocity*dt end
    if love.keyboard.isDown("left") then posX = posX -  velocity*dt end
    if love.keyboard.isDown("right") then posX = posX + velocity*dt end

    whale:moveTo(posX, posY)
end

function love.draw()
    whale:draw()
    love.graphics.print(math.ceil(posX) .. " " .. math.ceil(posY), 40, 40)
    love.graphics.print("Arguments: " .. #arg, 40, 60);
    love.graphics.print("Arg 1    : " .. arg[1], 40, 80);
    for i = 2, #arg do
        love.graphics.print("Arg " .. i .. "    : " .. arg[i], 40, 80 + (i - 1)*20);
    end
end