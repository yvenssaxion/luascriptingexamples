running = true
memory = {0, 0, 0}
pointer = 1
commands = {}
function commands.CHP(V) 
    pointer = V
end
function commands.ADD(V) 
    memory[pointer] = memory[pointer] + V 
end
function commands.MUL(V) 
    memory[pointer] = memory[pointer] * V 
end
function commands.PRI(V) 
    print(memory[pointer]) 
end
function commands.ADP(V)
    memory[pointer] = memory[pointer] + memory[V]
end
function commands.EXT(V)
    running = false
end

local opcode = ""
local operand = 0
local command = ""
while(running) do
    command = io.read()
    opcode, operand = string.match(command, "(%a+)%s*(%d+)")
    if not opcode then
        opcode = command
    end
    if operand then
        operand = tonumber(operand)
    end
    commands[opcode](operand or "")
end