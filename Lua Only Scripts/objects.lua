object = {}
object.name = "John Doe"
object.speak = function () print("I am speaking") end

object.speak()

object.createObject = function (name, speak)
    local newObject = {}
    newObject.name = name
    newObject.speak = function () print(speak) end
    return newObject
end

o = object.createObject("Jane Doe", "I'm shy")
o.speak()

object.createSpeakNameObject = function (name)
    local newObject = {}
    newObject.name = name
    newObject.speak = function () print("Hello, my name is "..name) end
    return newObject
end

p = object.createSpeakNameObject("Mary Doe")
p.speak()
-- Prints "Hello, my name is Mary Doe"

p.name = "Maryanne Doe"
p.speak()
-- Still prints "Hello, my name is Mary Doe"

object.createSpeakNameSelfObject = function (name)
    local newObject = {}
    newObject.name = name
    newObject.speak = function (self) print("Hello, my name is " .. self.name) end
    return newObject
end

q = object.createSpeakNameSelfObject("Juan Doe");
q.speak(q)
-- Prints "Hello, my name is Juan Doe"

q.name = "Don Juan Doe"
q.speak(q)
-- Prints "Hello, my name is Don Juan Doe"
q:speak()


GameObject = {identifier = "", renderable = true, position = {0.0, 0.0}}
function GameObject.render()
    if(GameObject.renderable) then
        -- Render object
        print("Rendering object " .. GameObject.identifier)
    end
end

function GameObject.print()
    print("GameObject " .. GameObject.identifier .. 
    "is " .. (GameObject.renderable and "" or "not ") .. "renderable, " ..
    "located at " .. GameObject.position[1] .. " ," .. GameObject.position[2])
end
          
GameObject.render()
-- Prints Rendering object
GameObject.print()
-- Prints GameObject is renderable, located at 0.0 ,0.0

GameObject = {identifier = "", renderable = true, position = {0.0, 0.0}}
function GameObject.render(self)
    if(self.renderable) then
        -- Render object
        print("Rendering object " .. self.identifier)
    end
end

function GameObject:print()
    print("GameObject " .. self.identifier .. 
    "is " .. (self.renderable and "" or "not ") .. "renderable, " ..
    "located at " .. self.position[1] .. " ," .. self.position[2])
end
          
GameObject:render()
-- Prints Rendering object
GameObject:print()
-- Prints GameObject is renderable, located at 0.0 ,0.0

metaobject = {}
setmetatable(object, metaobject)
print(getmetatable(object))
-- Print table: 0x7f883fc06cf0

-- A, B = {}
-- print (A .. B)
-- Print lua: objects.lua:93: attempt to 
-- concatenate a table value (global 'A')

concatArr = {
    __concat = function(arrA, arrB)
        local ret = {}
        for i, elem in pairs(arrA) do
            ret[#ret + 1] = elem
        end
        for i, elem in pairs(arrB) do
            ret[#ret + 1] = elem
        end
        return ret
    end
}

A = {1, 2, 3}
B = {12, 13, 17}

setmetatable(A, concatArr)
setmetatable(B, concatArr)
C = A .. B
for i, elem in pairs(C) do
    print(i .. " " .. C[i])
end

function GameObject:new()
    local newGameObject = {}
    setmetatable(newGameObject, self)
    self.__index = self
    return newGameObject
end

square = GameObject:new()
square:render()
-- Prints Rendering object

function GameObject:new(identifier, renderable, position)
    local newGameObject = {}
    setmetatable(newGameObject, self)
    self.__index = self
    newGameObject.identifier = identifier
    newGameObject.renderable = renderable
    newGameObject.position = position
    return newGameObject
end

cube = GameObject:new("Cube", true, {1.0, 4.0})
cube:render()
-- Prints Rendering object Cube

function privateObject(initialValue)
    local self = {value = initialValue}
    local updateValue = function(newValue)
                            self.value = newValue
                        end
    local getValue = function()
                            return self.value
                        end
    return {
        updateValue = updateValue,
        getValue = getValue
    }
end

p1 = privateObject(10)
print(p1.getValue())
-- Print 10
p1.updateValue(11)
print(p1.getValue())
-- Print 11

MeshObject = GameObject:new("", true, {0.0, 0.0})
function MeshObject:new(identifier, position)
    local newGameObject = GameObject:new(identifier, true, position)
    setmetatable(newGameObject, self)
    self.__index = self
    local program = {vertexShader = "", fragmentShader = ""}
    function newGameObject:getVertexShader() 
        return program.vertexShader 
    end
    function newGameObject:setVertexShader(vertexShader) 
        program.vertexShader = vertexShader
    end
    function newGameObject:getFragmentShader() 
        return program.fragmentShader 
    end
    function newGameObject:setFragmentShader(fragmentShader) 
        program.fragmentShader = fragmentShader
    end
    return newGameObject
end

mo = MeshObject:new("MO", {10.0, 20.0})
mo:render()
-- Prints Rendering object MO
mo:setVertexShader("render my vertices!")
print(mo:getVertexShader())
-- Prints render my vertices!