-- local function getNextPrimeNumber()
--     local currentNumber = 2
--     local isPrime = true

--     while(true) do
--         isPrime = true
--         local halfCurrentNumber = currentNumber / 2
--         for i = 2, halfCurrentNumber do
--             if(math.fmod(currentNumber, i) == 0) then
--                 isPrime = false
--                 do break end
--             end
--         end
--         if (isPrime) then
--             coroutine.yield(currentNumber)
--         end
--         currentNumber = currentNumber + 1
--     end
-- end

-- local function consumePrimeNumber(primeCount)
--     local getPrimeNumbers = coroutine.create(getNextPrimeNumber)
--     for i = 1, primeCount do
--         local success, prime = coroutine.resume(getPrimeNumbers)
--         print(i .. " prime: " .. prime)
--     end
-- end

-- consumePrimeNumber(10)

local function getNextPrimeNumber()
    local currentNumber = 2
    local isPrime = true

    while(true) do
        isPrime = true
        local halfCurrentNumber = currentNumber / 2
        for i = 2, halfCurrentNumber do
            if(math.fmod(currentNumber, i) == 0) then
                isPrime = false
                do break end
            end
        end
        if (isPrime) then
            coroutine.yield(currentNumber)
        end
        currentNumber = currentNumber + 1
    end
end

local function consumePrimeNumber(primeCount)
    local getPrimeNumbers = coroutine.wrap(getNextPrimeNumber)
    for i = 1, primeCount do
        local prime = getPrimeNumbers()
        print(i .. " prime: " .. prime)
    end
end

consumePrimeNumber(10)