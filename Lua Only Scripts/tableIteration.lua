-- Table Iteration

local function f(i, v) 
    print(i .. " " .. v) 
end

local function PrintPairsAndIpairs(t)
    print("Pairs")
    for i, v in pairs(t) do f(i, v) end

    print("IPairs")
    for i, v in ipairs(t) do f(i, v) end
end

local function TableExampleWithoutNil()
    -- No keys
    t1 = {"value1", "value2", "value3"}

    -- String keys
    t2 = {a = "value1", b = "value2", c = "value3"}

    -- Mixed keys
    t3 = {"value1", b = "value2", ["c"] = "value3"}

    print("T1")
    PrintPairsAndIpairs(t1)

    print("T2")
    PrintPairsAndIpairs(t2)

    print("T3")
    PrintPairsAndIpairs(t3)
end

local function TableExampleWithNil()
    -- No keys
    t1 = {"value1", "value2", "value3", nil, "value4"}

    -- String keys
    t2 = {a = "value1", b = "value2", nil, c = "value3", d = "value4"}

    -- Mixed keys
    t3 = {"value1", nil, b = "value2", ["c"] = "value3", d = "value4"}

    print("T1")
    PrintPairsAndIpairs(t1)

    print("T2")
    PrintPairsAndIpairs(t2)

    print("T3")
    PrintPairsAndIpairs(t3)
end

print("Table Example Without Nil...\n")
TableExampleWithoutNil()
print("\n")
print("Table Exame With Nil...\n")
TableExampleWithNil()
print("\n")