local running = true
local ret = nil
local msg = ""
while(running) do
    print("Enter a lua valid command:")
    local command = io.read()
    if (string.lower(command) == "exit") then
        running = false
    else
        ret, msg = load(command)
        if (ret) then
            ret()
        else
            print("Error: " .. msg)
        end
    end
end