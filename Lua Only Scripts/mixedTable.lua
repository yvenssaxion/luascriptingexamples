-- Mixed values
t1 = {["X"] = 12, "value1", [11] = 6, "value2", ["PowerPoint"] = "Cool", "value3", [4] = "value4", "value5"}

for i, v in pairs(t1) do print(i .. " " .. v) end

t1["PowerPoint"] = nil

for i, v in ipairs(t1) do print(i .. " " .. v) end