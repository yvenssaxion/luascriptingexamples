#!/usr/local/bin/lua
local exampleSwitch = { 
    ["1"] = function () print("Hi") end,
    ["2"] = function () print("Hello") end,
    ["default"] = function () print("Use the arguments (1 or 2) for different cases") end
}

local input = arg[1] or "default"
local case = exampleSwitch[input]
if(case) then
    case()
else
    exampleSwitch["default"]()
end