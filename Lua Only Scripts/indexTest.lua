-- A, B = {}
-- print (A .. B)
-- Print lua: objects.lua:93: attempt to 
-- concatenate a table value (global 'A')

concatArr = {
    __concat = function(arrA, arrB)
        local ret = {}
        for i, elem in pairs(arrA) do
            ret[#ret + 1] = elem
        end
        for i, elem in pairs(arrB) do
            ret[#ret + 1] = elem
        end
        return ret
    end,

    __index = function(table, key)
        print("Key not found")
        return 20
    end
}

A = {1, 2, 3}
B = {12, 13, 17}

setmetatable(A, concatArr)
setmetatable(B, concatArr)
-- C = A .. B
for i, elem in pairs(A) do
    print(i .. " " .. A[i])
end
print(A[33])
print("lll")
-- function GameObject:new()
--     local newGameObject = {}
--     setmetatable(newGameObject, self)
--     self.__index = self
--     return newGameObject
-- end

-- square = GameObject:new()
-- square:render()
-- -- Prints Rendering object

-- function GameObject:new(identifier, renderable, position)
--     local newGameObject = {}
--     setmetatable(newGameObject, self)
--     self.__index = self
--     newGameObject.identifier = identifier
--     newGameObject.renderable = renderable
--     newGameObject.position = position
--     return newGameObject
-- end

-- cube = GameObject:new("Cube", true, {1.0, 4.0})
-- cube:render()
-- -- Prints Rendering object Cube

-- function privateObject(initialValue)
--     local self = {value = initialValue}
--     local updateValue = function(newValue)
--                             self.value = newValue
--                         end
--     local getValue = function()
--                             return self.value
--                         end
--     return {
--         updateValue = updateValue,
--         getValue = getValue
--     }
-- end

-- p1 = privateObject(10)
-- print(p1.getValue())
-- -- Print 10
-- p1.updateValue(11)
-- print(p1.getValue())
-- -- Print 11

-- MeshObject = GameObject:new("", true, {0.0, 0.0})
-- function MeshObject:new(identifier, position)
--     local newGameObject = GameObject:new(identifier, true, position)
--     setmetatable(newGameObject, self)
--     self.__index = self
--     local program = {vertexShader = "", fragmentShader = ""}
--     function newGameObject:getVertexShader() 
--         return program.vertexShader 
--     end
--     function newGameObject:setVertexShader(vertexShader) 
--         program.vertexShader = vertexShader
--     end
--     function newGameObject:getFragmentShader() 
--         return program.fragmentShader 
--     end
--     function newGameObject:setFragmentShader(fragmentShader) 
--         program.fragmentShader = fragmentShader
--     end
--     return newGameObject
-- end

-- mo = MeshObject:new("MO", {10.0, 20.0})
-- mo:render()
-- -- Prints Rendering object MO
-- mo:setVertexShader("render my vertices!")
-- print(mo:getVertexShader())
-- -- Prints render my vertices!