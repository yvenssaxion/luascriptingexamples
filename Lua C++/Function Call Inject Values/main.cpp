#include <lua.hpp>
#include <iostream>

using namespace std;

int main() {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_loadfile(lua, "switchCase.lua");

    lua_pushstring(lua, "1");
    lua_setglobal(lua, "input");

    lua_call(lua, 0, 0);

    lua_close(lua);
    return 0;
}