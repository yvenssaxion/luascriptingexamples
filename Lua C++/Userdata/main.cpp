#include <stdio.h>
#include <lua.hpp>

class TestObject {
    public:
        int counter;
        int multiplier;
};

static int newTestObject(lua_State *lua) {
    int i, n;
    TestObject* object;
    int counter = luaL_checkinteger(lua, -2);
    int multiplier = luaL_checkinteger(lua, -1);
    size_t nbytes = sizeof(TestObject);
    object = static_cast<TestObject*>(lua_newuserdata(lua, nbytes));
    object->counter = counter;
    object->multiplier = multiplier;
    printf("-- %d\n", lua_gettop(lua));
    return 1;
}

static int updateCounter(lua_State *lua) {
    TestObject* object = static_cast<TestObject*>(lua_touserdata(lua, 1));
    object->counter++;
    return 0;
}

static int getCounter(lua_State *lua) {
    TestObject* object = static_cast<TestObject*>(lua_touserdata(lua, 1));
    lua_pushinteger(lua, object->counter);
    return 1;
}


static const struct luaL_Reg testObjectLib [] = {
    {"new", newTestObject},
    {"update", updateCounter},
    {"getCounter", getCounter},
    {NULL, NULL} // - signals the end of the registry
};

int main(void) {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_newlib(lua, testObjectLib);
    lua_setglobal(lua, "testObject");
    luaL_loadfile(lua, "userdata.lua");
    lua_call(lua, 0, 0);

    return 0;
}