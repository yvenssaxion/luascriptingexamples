#include <stdio.h>
#include <lua.hpp>

class TestObject {
    private:
        static TestObject* targetObject;
    public:
        static void updateTestObjectCounter(TestObject* object) {
            TestObject::targetObject = object;
        }
        static int updateCounter(lua_State* lua) {
            TestObject::targetObject->counter++;
            return 0;
        }
    public:
        int counter;
};

TestObject* TestObject::targetObject;

int main(void)
{
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    
    lua_pushcfunction(lua, TestObject::updateCounter);
    lua_setglobal(lua, "updateCounter");

    TestObject object;
    object.counter = 10;
    TestObject::updateTestObjectCounter(&object);

    luaL_loadfile(lua, "object.lua");
    lua_call(lua, 0, 0);

    printf("Object counter: %d\n", object.counter);
    return 0;
}