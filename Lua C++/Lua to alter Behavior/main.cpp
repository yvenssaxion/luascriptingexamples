#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <lua.hpp>

struct pos {
    float x, y, z;
    pos(float x, float y, float z) : x(x), y(y), z(z) {}
};

static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

int main(void)
{
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_loadfile(lua, "behavior.lua");
    lua_call(lua, 0, 0);

    GLFWwindow* window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(600, 400, "Lua to alter Behavior", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    float ratio;
    int width = 0;
    int height = 0;
    while (!glfwWindowShouldClose(window))
    {
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glBegin(GL_TRIANGLES);

        glColor3f(1.f, 0.f, 0.f);
        lua_getglobal(lua, "trianglePositions");
        pos left(-0.6f, -0.4f, 0.f);
        lua_pushnumber(lua, left.x);
        lua_pushnumber(lua, left.y);
        lua_pushnumber(lua, left.z);
        lua_pushnumber(lua, glfwGetTime());
        lua_call(lua, 4, 3);
        left.z = lua_tonumber(lua, -1); lua_pop(lua, 1);
        left.y = lua_tonumber(lua, -1); lua_pop(lua, 1);
        left.x = lua_tonumber(lua, -1); lua_pop(lua, 1);
        glVertex3f(left.x, left.y, left.z);

        glColor3f(0.f, 1.f, 0.f);
        lua_getglobal(lua, "trianglePositions");
        pos right(0.6f, -0.4f, 0.f);
        lua_pushnumber(lua, right.x);
        lua_pushnumber(lua, right.y);
        lua_pushnumber(lua, right.z);
        lua_pushnumber(lua, glfwGetTime());
        lua_call(lua, 4, 3);
        right.z = lua_tonumber(lua, -1); lua_pop(lua, 1);
        right.y = lua_tonumber(lua, -1); lua_pop(lua, 1);
        right.x = lua_tonumber(lua, -1); lua_pop(lua, 1);
        glVertex3f(right.x, right.y, right.z);

        glColor3f(0.f, 0.f, 1.f);
        lua_getglobal(lua, "trianglePositions");
        pos top(0.f, 0.6f, 0.f);
        lua_pushnumber(lua, top.x);
        lua_pushnumber(lua, top.y);
        lua_pushnumber(lua, top.z);
        lua_pushnumber(lua, glfwGetTime());
        lua_call(lua, 4, 3);
        top.z = lua_tonumber(lua, -1); lua_pop(lua, 1);
        top.y = lua_tonumber(lua, -1); lua_pop(lua, 1);
        top.x = lua_tonumber(lua, -1); lua_pop(lua, 1);
        glVertex3f(top.x, top.y, top.z);

        glEnd();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    lua_close(lua);
    exit(EXIT_SUCCESS);
}