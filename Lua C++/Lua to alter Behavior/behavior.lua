function trianglePositions(x, y, z, time)
    return x * math.sin(time), y + 0.5 * math.cos(time), z
end