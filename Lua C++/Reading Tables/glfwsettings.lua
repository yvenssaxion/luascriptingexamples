-- Window property
aspectRatio = 4 / 3
forceAspectRatio = false

-- Window Width
windowTable = {}
windowTable.width = 600 

 -- Window Height
if(forceAspectRatio) then
    windowTable.height = math.floor(width / aspectRatio)
else
    windowTable.height = 600
end

-- Window Title
title = "OpenGL + GLFW Window!"

-- Properties regarding the animation
-- Velocity in which the triangle will spin
spinning = 1.0