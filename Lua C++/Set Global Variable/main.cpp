#include <lua.hpp>
#include <iostream>

using namespace std;

int main() {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_loadfile(lua, "globalvariable.lua");

    lua_pushinteger(lua, 10);
    lua_setglobal(lua, "grade");

    lua_call(lua, 0, 0);

    lua_close(lua);
    return 0;
}