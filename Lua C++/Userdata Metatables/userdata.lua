o = testObject.new(1, 1)
print(o)
-- Prints userdata: 0x100106168
print(getmetatable(o))
-- Prints cmgt.testObject: 0x7fb45ec00d48
print(o:getCounter())
-- Prints 1
o:update()
o:update()
print(o:getCounter())
-- Prints 3