#include <stdio.h>
#include <lua.hpp>
#include <new>

class TestObject {
    public:
        int counter;
        int multiplier;

    public:
        TestObject(int counter, int multiplier) : counter(counter), multiplier(multiplier)
        { }
};

static int newTestObject(lua_State *lua) {
    TestObject* object ;
    int counter = luaL_checkinteger(lua, -2);
    int multiplier = luaL_checkinteger(lua, -1);
    size_t nbytes = sizeof(TestObject);
    object = new((lua_newuserdata(lua, nbytes))) TestObject(counter, multiplier);

    luaL_getmetatable(lua, "cmgt.testObject");
    lua_setmetatable(lua, -2);
    return 1;
}

static int updateCounter(lua_State *lua) {
    TestObject* object = static_cast<TestObject*>(
        luaL_checkudata(lua, 1, "cmgt.testObject"));
    object->counter++;
    return 0;
}

static int getCounter(lua_State *lua) {
    TestObject* object = static_cast<TestObject*>(
        luaL_checkudata(lua, 1, "cmgt.testObject"));
    lua_pushinteger(lua, object->counter);
    return 1;
}

static const struct luaL_Reg testObjectMetalib [] = {
    {"update", updateCounter},
    {"getCounter", getCounter},
    {NULL, NULL} // - signals the end of the registry
};

static const struct luaL_Reg testObjectLib [] = {
    {"new", newTestObject},
    {NULL, NULL} // - signals the end of the registry
};

int main(void) {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);

    luaL_newmetatable(lua, "cmgt.testObject");

    lua_pushstring(lua, "__index");
    lua_pushvalue(lua, -2); //Pushes the metatable
    lua_settable(lua, -3);

    luaL_setfuncs(lua, testObjectMetalib, 0);

    luaL_newlib(lua, testObjectLib);
    luaL_getmetatable(lua, "cmgt.testObject");
    lua_setmetatable(lua, -2);
    lua_setglobal(lua, "testObject");

    luaL_loadfile(lua, "userdata.lua");
    lua_call(lua, 0, 0);

    return 0;
}