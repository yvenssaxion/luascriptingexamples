-- Window property
aspectRatio = 4 / 3
forceAspectRatio = true

-- Window Width
width = 600 

 -- Window Height
if(forceAspectRatio) then
    height = math.floor(width / aspectRatio)
else
    height = 600
end

-- Window Title
title = "OpenGL + GLFW Window!"

-- Properties regarding the animation
-- Velocity in which the triangle will spin
spinning = 5.0 * math.pi



