#include <lua.hpp>
#include <iostream>

using namespace std;

int main() {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_loadfile(lua, "globalvariable.lua");

    lua_call(lua, 0, 0);

    lua_getglobal(lua, "grade");
    if(lua_isnumber(lua, -1)) {
        int grade = lua_tonumber(lua, -1);
        printf("The grade is %d\n", grade);
    }
    lua_pop(lua, 1);

    lua_close(lua);
    return 0;
}

