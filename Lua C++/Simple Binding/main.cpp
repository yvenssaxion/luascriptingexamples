#include <lua.hpp>

int main() {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_loadfile(lua, "helloworld.lua");

    lua_call(lua, 0, 0);

    lua_close(lua);
    return 0;
}