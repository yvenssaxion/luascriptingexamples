#include <stdio.h>
#include <lua.hpp>

int main(void)
{
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_loadfile(lua, "tables.lua");

    lua_call(lua, 0, 0);

    // Set the table to the top of the stack
    lua_getglobal(lua, "readTable");

    lua_pushinteger(lua, 1);
    lua_gettable(lua, -2);
    int value = lua_tonumber(lua, -1);
    lua_pop(lua, 1);
    printf("Value is: %d\n", value);

    lua_pushinteger(lua, 2);
    lua_pushinteger(lua, 4242);
    lua_settable(lua, -3);

    lua_pushinteger(lua, 2);
    lua_gettable(lua, -2);
    value = lua_tonumber(lua, -1);
    lua_pop(lua, 1);
    printf("Value is: %d\n", value);

    lua_pushinteger(lua, 3);
    lua_createtable(lua, 1, 0);
    lua_pushinteger(lua, 1);
    lua_pushinteger(lua, 424242);
    lua_settable(lua, -3);
    lua_settable(lua, -3);

    lua_pushinteger(lua, 3);
    lua_gettable(lua, -2);
    bool isTable = lua_istable(lua, -1);
    lua_pushinteger(lua, 1);
    lua_gettable(lua, -2);
    value = lua_tonumber(lua, -1);
    lua_pop(lua, 1);
    printf("Value is: %d\n", value);
    lua_pop(lua, 1);

    // Pop table
    lua_pop(lua, 1);

    return 0;
}