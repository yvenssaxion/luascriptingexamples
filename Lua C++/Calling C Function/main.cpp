#include <stdio.h>
#include <lua.hpp>

#include <math.h>

class TestObject {
    private:
        static TestObject* targetObject;
    public:
        static void updateTestObjectCounter(TestObject* object) {
            TestObject::targetObject = object;
        }

        static int updateCounter(lua_State* lua) {
            TestObject::targetObject->counter++;
            return 0;
        }

        int updateSelfCounter(lua_State* lua) {
            this->counter++;
            return 0;
        }

    public:
        int counter;
};

//Declare targetObject static variable
TestObject* TestObject::targetObject;

static int l_sin (lua_State* lua) {
    double d = lua_tonumber(lua, -1);
    lua_pushnumber(lua, sin(d));
    return 1;
}

static int l_cos (lua_State* lua) {
    double d = lua_tonumber(lua, -1);
    lua_pushnumber(lua, cos(d));
    return 1;
}

static int l_tan (lua_State* lua) {
    double d = lua_tonumber(lua, -1);
    lua_pushnumber(lua, tan(d));
    return 1;
}

#include <string>
std::string mysinName = "mysin";
std::string myName = "my";

static const struct luaL_Reg mylib [] = {
    {mysinName.c_str(), l_sin},
    {myName.append("cos").c_str(), l_cos},
    {"mytan", l_tan},
    {NULL, NULL} // - signals the end of the registry
};

int main(void)
{
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    
    luaL_newlib(lua, mylib);
    lua_setglobal(lua, "mylib");

    lua_pushcfunction(lua, l_sin);
    lua_setglobal(lua, "mysin");
    luaL_loadfile(lua, "mysin.lua");

    lua_call(lua, 0, 0);

    lua_getglobal(lua, "printMySin");
    lua_pushnumber(lua, 60);
    lua_call(lua, 1, 0);

    lua_getglobal(lua, "myCModule");
    lua_pushstring(lua, "cos");
    lua_pushcfunction(lua, l_cos);
    lua_settable(lua, -3);

    lua_pop(lua, -1);
    lua_getglobal(lua, "printMyCos");
    lua_pushnumber(lua, 60);
    lua_call(lua, 1, 0);

    // lua_pushcfunction(lua, TestObject::updateCounter);
    // lua_setglobal(lua, "updateCounter");
    // TestObject object;
    // object.counter = 10;
    // TestObject::updateTestObjectCounter(&object);

    // // lua_pushcfunction(lua, object.updateSelfCounter);
    // // lua_setglobal(lua, "updateSelfCounter");

    // lua_call(lua, 0, 0);


    // luaL_loadfile(lua, "userdata.lua");
    // lua_call(lua, 0, 0);

    // printf("Object counter: %d\n", object.counter);

    // luaL_loadfile(lua, "userdata.lua");
    // lua_call(lua, 0, 0);

    // printf("Object counter: %d\n", object.counter);

    // TestObject anotherObject;
    // anotherObject.counter = 5;
    // TestObject::updateTestObjectCounter(&anotherObject);

    // luaL_loadfile(lua, "userdata.lua");
    // lua_call(lua, 0, 0);

    // printf("Another object counter: %d\n", anotherObject.counter);
    return 0;
}