#include <lua.hpp>

int main() {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
    luaL_loadfile(lua, "mouseclick.lua");

    lua_call(lua, 0, 0);

    lua_getglobal(lua, "MouseClick");
    lua_pushinteger(lua, 20);
    lua_pushinteger(lua, 30);
    lua_pushstring(lua, "Middle Button");

    lua_call(lua, 3, 4);
    
    int top = lua_gettop(lua);
    for(int i = 1; i <= top; i++) {
        if(!lua_isnil(lua, -1)) {
            int value = lua_tonumber(lua, -1);
            printf("[%d] = %d\n", i, value);
            lua_pop(lua, 1);
        }
    }

    lua_close(lua);
    return 0;
}

